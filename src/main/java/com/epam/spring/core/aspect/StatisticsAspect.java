package com.epam.spring.core.aspect;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class StatisticsAspect {

	static Logger logger = LogManager.getLogger(StatisticsAspect.class);
	private Map<Class<?>, Integer> counter = new HashMap<>();

	@Pointcut("execution(* *.logEvent(..))")
	private void allLogEventMethods() {
	}

	@Before("allLogEventMethods()")
	public void logBefore(JoinPoint joinPoint) {
		logger.info("BEFORE : "
				+ joinPoint.getTarget().getClass().getSimpleName() + " "
				+ joinPoint.getSignature().getName());
	}

	@AfterReturning(pointcut = "allLogEventMethods()", returning = "retVal")
	public void logAfter(Object retVal) {
		logger.info("Returned value: " + retVal);
	}

	@AfterReturning("allLogEventMethods()")
	public void count(JoinPoint jp) {
		Class<?> clazz = jp.getTarget().getClass();
		if (!counter.containsKey(clazz)) {
			counter.put(clazz, 0);
		}
		counter.put(clazz, counter.get(clazz) + 1);
	}

	public void printStatistics() {
		for (Entry<Class<?>, Integer> entry : counter.entrySet()) {
			System.out.println("Logger: " + entry.getKey().getSimpleName()
					+ " - " + "Count: " + entry.getValue());
		}
	}
}
