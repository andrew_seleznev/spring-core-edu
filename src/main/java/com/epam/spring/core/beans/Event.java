package com.epam.spring.core.beans;

import java.text.DateFormat;
import java.util.Date;

public class Event {
	
	private int id = (int)(Math.random() * 100);
	private String msg;
	private Date date;
	private DateFormat df;
	
	public Event(Date date, DateFormat df) {
		this.date = date;
		this.df = df;
	}

	
	@Override
	public String toString() {
		return String.format("Event [id=%s, msg=%s, date=%s, df=%s]\n", id, msg, date, df);
	}

}
