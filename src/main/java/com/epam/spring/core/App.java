package com.epam.spring.core;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.spring.core.aspect.StatisticsAspect;
import com.epam.spring.core.beans.Client;
import com.epam.spring.core.beans.Event;
import com.epam.spring.core.enums.EventType;
import com.epam.spring.core.loggers.EventLogger;

public class App {

	private EventLogger defaultLogger;
	private Map<EventType, EventLogger> loggers;
	private Client client;
	
	@Autowired
	@Qualifier("statisticsAspect")
	private StatisticsAspect statisticsAspect;
	
	private void logEvent(EventType type, Event event) {
		EventLogger logger = loggers.get(type);
		if (logger == null) {
			logger = defaultLogger;
		}
		logger.logEvent(event);
		System.out.println("Client: " + client);
	}

	public App(Client client, EventLogger defaultLogger, Map<EventType, EventLogger> loggers) {
		this.defaultLogger = defaultLogger;
		this.loggers = loggers;
		this.client = client;
	}

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
		App app = (App) ctx.getBean("app");
		for (int i = 0; i < 8; i++) {
			try {
				app.logEvent(null, (Event) ctx.getBean("event"));
				Thread.currentThread().sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < 8; i++) {
			try {
				app.logEvent(EventType.INFO, (Event) ctx.getBean("event"));
				Thread.currentThread().sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < 8; i++) {
			try {
				app.logEvent(EventType.ERROR, (Event) ctx.getBean("event"));
				Thread.currentThread().sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		ctx.close();
	}
	
	public void destroy() {
		statisticsAspect.printStatistics();
	}
}
