package com.epam.spring.core.loggers;

import java.util.Iterator;
import java.util.List;

import com.epam.spring.core.beans.Event;

public class CacheFileEventLogger extends FileEventLogger {

	private int cacheSize;
	private List<Event> cache;

	public CacheFileEventLogger(String filename, int cacheSize, List<Event> cache) {
		super(filename);
		this.cacheSize = cacheSize;
		this.cache = cache;
	}

	@Override
	public void logEvent(Event event) {
		cache.add(event);
		if (cache.size() == cacheSize) {
			writeEventsFromCache();
			cache.clear();
		}
	}

	public void destroy() {
		if (!cache.isEmpty()) {
			writeEventsFromCache();
		}
	}
	
	private void writeEventsFromCache() {
		for (Iterator<Event> iterator = cache.iterator(); iterator.hasNext();) {
			Event event = (Event) iterator.next();
			super.logEvent(event);
		}
	}

}
