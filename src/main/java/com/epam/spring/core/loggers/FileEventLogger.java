package com.epam.spring.core.loggers;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.epam.spring.core.beans.Event;

public class FileEventLogger implements EventLogger {
	
	private String fileName;
	private File file;
	
	public FileEventLogger(String filename) {
		this.fileName = filename;
	}

	public void logEvent(Event event) {
		try {
			FileUtils.writeStringToFile(file, event.toString(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void init() throws IOException {
		this.file = new File(fileName);
		if (!this.file.canWrite()) {
			throw new IOException();
		}
	}

}
