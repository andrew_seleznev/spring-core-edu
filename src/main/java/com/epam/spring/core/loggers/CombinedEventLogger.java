package com.epam.spring.core.loggers;

import java.util.Collection;

import com.epam.spring.core.beans.Event;

public class CombinedEventLogger implements EventLogger {
	
	private Collection<EventLogger> loggers;
	
	public CombinedEventLogger(Collection<EventLogger> loggers) {
		this.loggers = loggers;
	}
	
	public void logEvent(Event event) {
		for (EventLogger eventLogger : loggers) {
			eventLogger.logEvent(event);
		}
	}

}
